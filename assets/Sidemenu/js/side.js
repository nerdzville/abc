if (screen.width <= 767) {
    document.getElementById("hover").setAttribute("onmouseover", "msmenu_show()");
    document.getElementById("hover").setAttribute("onmouseout", "msmenu_hide()");

    function msmenu_show() {
        document.getElementById("hover").style.left = "-" + 35 + "px";
    }

    function msmenu_hide() {
        document.getElementById("hover").style.left = "-" + 232 + "px";
    }
} else {
    function smenu_show() {
        document.getElementById("hover").style.left = 40 + "px"
        document.getElementById("img").setAttribute("src", "img/ic_keyboard_arrow_left_black_24dp_2x.png");
    }

    function smenu_hide() {
        document.getElementById("hover").style.left = "-" + 190 + "px";
        document.getElementById("img").setAttribute("src", "img/ic_keyboard_arrow_right_black_24dp_2x.png");
    }
}
