<?php

session_start();

//require 'backend/afkamain.php';

?>



<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Apply for Afkanerd</title>
    <link rel="shortcut icon" href="img/LOGO%20HEAD.png" />
    <link rel="stylesheet" href="assets/Bootstrap4/css/bootstrap.css" />
    <link rel="stylesheet" href="assets/css/style.css" />
    <link rel="stylesheet" href="assets/Sidemenu/css/side.css" />
    <link rel="stylesheet" href="assets/loader/css/loader.css" />
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
</head>

<style>
    a:hover {
        color: blue;
        text-decoration: none;
        cursor: pointer;
    }

    a,
    p,
    h1,
    h3,
    select,
    span,
    body {
        color: black;
        text-decoration: none;
        font-family: 'Roboto', sans-serif;
        font-weight: 300;
    }

</style>

<body style="background-image: url(img/desktop-hd-orange-abstract-backgrounds.jpg)">

    <!--page_loader_start-->
    <div id="con1">
        <div id="con2">
            <!--nothing here...-->
        </div>
    </div>
    <!--page_loader_end-->

    <!--main_container_start-->
    <div class="container_fluid" id="holder">

        <!--navbar_start-->
        <nav class="navbar navbar-expand-lg navbar-light" id="nav">

            <!--Branding-->
            <nav class="navbar navbar-light">
                <a class="navbar-brand" href="https://afkanerd.com">
                    <!--logo_here-->
                    <img src="img/ic_arrow_back_black_24dp_2x.png" width="30" height="30" class="d-inline-block align-top" alt="">
                </a>
            </nav>

        </nav>
        <!--navbar_end-->

        <!--side_menu_start-->
        <div onmouseover="smenu_show()" onmouseout="smenu_hide()" id="static">
            <img src="img/ic_keyboard_arrow_right_black_24dp_2x.png" id="img" width="30" height="30" class="d-inline-block m-auto" alt="">
        </div>

        <div onmouseover="smenu_show()" onmouseout="smenu_hide()" id="hover">
            <ul>
                <li>info@afkanerd.com</li>
                <hr />
                <li>(0237) 652156811</li>
                <hr />
                <li>We expect you</li>
            </ul>
        </div>
        <!--side_menu_end-->

        <!--content_start-->
        <div class="container-fluid">

            <!--info about internship-->
            <div class="row">
                <div class="col-md-6 mx-auto" id="info">
                    <h1 class="display-4">Internship</h1>
                    <p class="m-0">This is some information about the internship</p>
                </div>
            </div>

            <!--requirements-->
            <div class="row">
                <div class="col-md-4 mx-auto">
                    <div class="jumbotron" id="req">
                        <h3 style="text-align: left"><u>Requirements</u></h3>
                        <ul style="text-align: justify">
                            <li>A laptop</li>
                            <li>An email account (gmail best)</li>
                            <li>Be a vivid reader</li>
                            <li>Have capabilities to follow working standards</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 mx-auto" id="content">
                  <?php if($_GET['state'] == 'false') { ?>
                  <div class="alert alert-danger" role="alert">
                    Please make sure you haven't applied before with your email or phone number and every field is filled correctly!
                  </div>
                <?php } ?>
                  <?php if($_SESSION['information']) { ?>
                    <form method="post" action="backend/afkamain.php">
                        <div class="form-group">
                            <label class="col-form-label">First name</label>
                            <input required type="text" class="form-control" name="first_name" value="<?php echo $_SESSION['information']['first_name']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Last name</label>
                            <input required type="text" class="form-control" name="last_name" value="<?php echo $_SESSION['information']['last_name']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">University</label>
                            <input required type="text" class="form-control" name="university" value="<?php echo $_SESSION['information']['university']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">University website</label>
                            <input required type="text" class="form-control" name="university_website" value="<?php echo $_SESSION['information']['university_website']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Matricule number</label>
                            <input required type="text" class="form-control" name="matricule_number" value="<?php echo $_SESSION['information']['matricule_number']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Level</label>
                            <input required type="text" class="form-control" name="level" value="<?php echo $_SESSION['information']['level']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Course/Deparment</label>
                            <input required type="text" class="form-control" name="course_department" value="<?php echo $_SESSION['information']['course_department']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input required type="email" class="form-control" name="email" value="<?php echo $_SESSION['information']['email']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Phone Number</label>
                            <input required type="text" class="form-control" name="phone_number" value="<?php echo $_SESSION['information']['phone_number']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Country</label>
                            <input required type="text" class="form-control" name="country" value="<?php echo $_SESSION['information']['country']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Gender</label>
                            <select class="form-control" name="gender">
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Other</option>
                                </select>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Age</label>
                            <input required type="text" class="form-control" name="age" value="<?php echo $_SESSION['information']['age']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Village</label>
                            <input required type="text" class="form-control" name="village" value="<?php echo $_SESSION['information']['village']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Experience</label>
                            <textarea class="form-control" rows="5" name="experience" ><?php echo $_SESSION['information']['experience']; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Achievement</label>
                            <textarea class="form-control" rows="5" name="achievement"><?php echo $_SESSION['information']['achievement']; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Start date</label>
                            <input required type="date" class="form-control" name="start_date" value="<?php echo $_SESSION['information']['start_date']; ?>" placeholder='Y-m-d'>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">End date</label>
                            <input required type="date" class="form-control" name="end_date"  placeholder='Y-m-d' value="<?php echo $_SESSION['information']['end_date']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Needle in haystack</label>
                            <input required type="text" class="form-control" name="inquiry" value="<?php echo $_SESSION['information']['inquiry']; ?>">
                        </div>

                        <!--submit_button-->
                        <input type="submit" class="btn btn-primary btn-block w-50 mx-auto" value="submit"></input>
                    </form>
                  <?php }  else { ?>

                    <form method="post" action="backend/afkamain.php">
                        <div class="form-group">
                            <label class="col-form-label">First name</label>
                            <input required type="text" class="form-control" name="first_name" placeholder="<?php echo $_SESSION['information']['first_name']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Last name</label>
                            <input required type="text" class="form-control" name="last_name" placeholder="<?php echo $_SESSION['information']['last_name']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">University</label>
                            <input required type="text" class="form-control" name="university" placeholder="<?php echo $_SESSION['information']['university']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">University website</label>
                            <input required type="text" class="form-control" name="university_website" placeholder="<?php echo $_SESSION['information']['university_website']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Matricule number</label>
                            <input required type="text" class="form-control" name="matricule_number" placeholder="<?php echo $_SESSION['information']['matricule_number']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Level</label>
                            <input required type="text" class="form-control" name="level" placeholder="<?php echo $_SESSION['information']['level']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Course/Deparment</label>
                            <input required type="text" class="form-control" name="course_department" placeholder="<?php echo $_SESSION['information']['course_department']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Email</label>
                            <input required type="email" class="form-control" name="email" placeholder="<?php echo $_SESSION['information']['email']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Phone Number</label>
                            <input required type="text" class="form-control" name="phone_number" placeholder="<?php echo $_SESSION['information']['phone_number']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Country</label>
                            <input required type="text" class="form-control" name="country" placeholder="<?php echo $_SESSION['information']['country']; ?>">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Gender</label>
                            <select class="form-control" name="gender">
                                    <option>Male</option>
                                    <option>Female</option>
                                    <option>Other</option>
                                </select>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Age</label>
                            <input required type="text" class="form-control" name="age" placeholder="Age">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Village</label>
                            <input required type="text" class="form-control" name="village" placeholder="Village">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Experience</label>
                            <textarea class="form-control" rows="5" name="experience" placeholder="Tell us about your experience as a coder/engineer/scientist or any cool things" ></textarea>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Achievement</label>
                            <textarea class="form-control" rows="5" name="achievement" placeholder="Please tell me what you're hoping to achieve during this internship"><?php echo $_SESSION['information']['achievement']; ?></textarea>
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Start date</label>
                            <input required type="date" class="form-control" name="start_date" placeholder="(Y-m-d) When you starting the internship">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">End date</label>
                            <input required type="date" class="form-control" name="end_date" placeholder="(Y-m-d) When you ending the internship">
                        </div>

                        <div class="form-group">
                            <label class="col-form-label">Needle in haystack</label>
                            <input required type="text" class="form-control" name="inquiry" placeholder="How did you find us">
                        </div>

                        <!--submit_button-->
                        <input type="submit" class="btn btn-primary btn-block w-50 mx-auto" value="submit"></input>
                    </form>
                  <?php } ?>
                </div>
            </div>
        </div>
        <!--content_side_end-->

        <!--footrt_start-->
        <footer id="footer">
            <p class="m-0">Afkanerd &copy;2018</p>
        </footer>
        <!--footrt_end-->

    </div>
    <!--main_container_end-->

    <!--jquery_start-->
    <script src="assets/Jquery/jquery-3.3.1.js"></script>
    <!--jquery_end-->

    <!--Bootstrap_js_start-->
    <script src="assets/Bootstrap4/js/bootstrap.js"></script>
    <!--Bootstrap_js_end-->

    <!--side_menu_js_start-->
    <script src="assets/Sidemenu/js/side.js"></script>
    <!--side_menu_js_end-->

    <!--loader_js_start-->
    <script src="assets/loader/js/loader.js"></script>
    <!--loader_js_end-->

    <!--custom_js_start-->
    <script src="assets/js/script.js"></script>
    <!--custom_js_end-->
</body>

</html>
